﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Core;


namespace Projet_Mimir
{
    public sealed partial class ListeEnigmes : Page
    {
        private Enigmes enigmes = (Enigmes)Application.Current.Resources["enigmes"];

        public ListeEnigmes()
        {
            this.InitializeComponent();
            listeEnigmes.DataContext = enigmes;
            Frame rootFrame = Window.Current.Content as Frame;
            rootFrame.Navigated += OnNavigated;
        }


        private void listeContact_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
            if (listeEnigmes.SelectedItem != null)
            {
                Enigme enigme = (Enigme)listeEnigmes.SelectedItem;
                if (enigme.Debloquee) {
                    openPagePlay(enigme.Classe);
                }
            }
        }

        void OnNavigated(Object sender, NavigationEventArgs e)
        {
            if (((Frame)sender).CanGoBack)
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            }
            else
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            }
        }


        void openPagePlay(Type pageName)
        {
            Frame.Navigate(pageName);
        }
    }
}

