﻿using System.Collections.ObjectModel;

namespace Projet_Mimir
{
    public class Enigmes : ObservableCollection<Enigme>
    {
        public Enigmes()
        {
            Add(new Enigme(1, "Tic Tac, tourne l’heure", typeof(Tic_tac), true));
            Add(new Enigme(2, "Fifty-fifty", typeof(Fifty_fifty)));
            Add(new Enigme(3, "Au plus profond de l’âme", typeof(Regard)));
        }
    }
}
