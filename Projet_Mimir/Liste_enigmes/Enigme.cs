﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projet_Mimir
{
    public class Enigme
    {
        private int numero;
        private string nom;
        private Type classe;
        private bool debloquee;


        /*Constructeur*/
        public Enigme(int unNumero, string unNom, Type uneClasse, bool uneDebloquee = false)
        {
            numero = unNumero;
            nom = unNom;
            classe = uneClasse;
            debloquee = uneDebloquee;
        }

        /*Propriétés*/
        public int Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }

        public Type Classe
        {
            get { return classe; }
            set { classe = value; }
        }

        public bool Debloquee
        {
            get { return debloquee; }
            set { debloquee = value; }
        }

        /*ToString*/
        public override string ToString()
        {
            return "Enigme " + Numero + " - " + Nom ;
        }
    }
}
