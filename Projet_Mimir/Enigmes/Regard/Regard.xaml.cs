﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour plus d'informations sur le modèle d'élément Page vierge, voir la page http://go.microsoft.com/fwlink/?LinkId=234238

namespace Projet_Mimir
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class Regard : Page
    {
        private static Enigmes enigmes = (Enigmes)Application.Current.Resources["enigmes"];
        public Regard()
        {
            this.InitializeComponent();
        }

        private void btn_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxReponse.Text.Contains("Abyme"))
            {
                Application.Current.Resources["enigmes"] = enigmes;
                Frame.Navigate(typeof(MainPage));
            }
            else
            {
                textBlockMauvReponse.Visibility = Visibility.Visible;
            }
        }

        private void textBoxReponse_textChanged(object sender, TextChangedEventArgs e)
        {
            textBlockMauvReponse.Visibility = Visibility.Collapsed;
        }
    }
}
