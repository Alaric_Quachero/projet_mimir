﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Core;

namespace Projet_Mimir
{
    
    public sealed partial class Tic_tac : Page
    {
        private static Enigmes enigmes = (Enigmes)Application.Current.Resources["enigmes"];

        public Tic_tac()
        {
            this.InitializeComponent();
            SystemNavigationManager.GetForCurrentView().BackRequested += OnBackRequested;
            Frame rootFrame = Window.Current.Content as Frame;
            rootFrame.Navigated += OnNavigated;

            indice.Text = "Dans une minute, il faudra de nouveau activer vos méninges pour trouver ce code !";
        }

        /**/
        private void btn_Valider(object sender, RoutedEventArgs e)
        {
            string code = recuperer_code();
            verifierCode(code);
        }

        private void btn_Click_effacer(object sender, RoutedEventArgs e)
        {
            textBox1.Text = "*";
            textBox2.Text = "*";
            textBox3.Text = "*";
            textBox4.Text = "*";
        }


        private string recuperer_code()
        {
            return textBox1.Text + textBox2.Text + textBox3.Text + textBox4.Text;
        }

        /**/
        public void verifierCode(string code)
        {
            string bonCode = DateTime.Now.ToString("hhmm");
            if (bonCode == code)
            {
                enigmes[1].Debloquee = true;
                Application.Current.Resources["enigmes"] = enigmes;
                
                Frame.Navigate(typeof(Fifty_fifty));
            }
            else if (bonCode != code)
            {
                myMediaElement.Play();
                phraseEchec.Text = "Mauvais code ! Retente ta chance !";
            }
        }
           
        private void btn_keybord(object sender, RoutedEventArgs e)
        {
            if (textBox1.Text == "*")
                textBox1.Text = ((Button)sender).Content.ToString();
            else if (textBox2.Text == "*")
                textBox2.Text = ((Button)sender).Content.ToString();
            else if (textBox3.Text == "*")
                textBox3.Text = ((Button)sender).Content.ToString();
            else if (textBox4.Text == "*")
                textBox4.Text = ((Button)sender).Content.ToString();
        }

        private void OnNavigated(Object sender, NavigationEventArgs e)
        {
            if (((Frame)sender).CanGoBack)
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            }
            else
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            }
        }

        private void OnBackRequested(object sender, BackRequestedEventArgs e)
        {
            Frame rootFrame = Window.Current.Content as Frame;
            if (rootFrame.CanGoBack) { e.Handled = true; rootFrame.GoBack(); }
        }
    }
}

