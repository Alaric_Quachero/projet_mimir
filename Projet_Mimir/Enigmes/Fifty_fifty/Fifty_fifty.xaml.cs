﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Projet_Mimir
{
    
    public sealed partial class Fifty_fifty : Page
    {
        private static Enigmes enigmes = (Enigmes)Application.Current.Resources["enigmes"];
        public Fifty_fifty()
        {
            this.InitializeComponent();
        }

        /**/
        private void btn_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxReponse.Text.Contains("Partage"))
            {
                enigmes[2].Debloquee = true;
                Application.Current.Resources["enigmes"] = enigmes;
                Frame.Navigate(typeof(Regard));
            }
            else
            {
                textBlockMauvReponse.Visibility = Visibility.Visible;
            }
        }

        private void image_tapped2(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            changerImage();
        }

        private void image_tapped1(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            changerImage();
        }
  
        public void changerImage()
        {
            if (pictureBox.Visibility == Visibility.Visible)
            {
                pictureBox.Visibility = Visibility.Collapsed;
                pictureBox2.Visibility = Visibility.Visible;
            }
            else if (pictureBox.Visibility != Visibility.Visible)
            {
                pictureBox.Visibility = Visibility.Visible;
                pictureBox2.Visibility = Visibility.Collapsed;
            }
        }

        private void textBoxReponse_textChanged(object sender, TextChangedEventArgs e)
        {
            textBlockMauvReponse.Visibility = Visibility.Collapsed;
        }
    }
}

