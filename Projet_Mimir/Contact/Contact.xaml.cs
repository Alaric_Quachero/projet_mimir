﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Core;


namespace Projet_Mimir
{
    
    public sealed partial class Contact : Page
    {
        public PersonneViewModel ViewModel { get; set; }

        public Contact()
        {
            this.InitializeComponent();
            this.ViewModel = new PersonneViewModel();
            Frame rootFrame = Window.Current.Content as Frame;
            rootFrame.Navigated += OnNavigated;
        }

        private void OnNavigated(Object sender, NavigationEventArgs e)
        {
            if (((Frame)sender).CanGoBack)
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;
            }
            else
            {
                SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Collapsed;
            }
        }

        private void listeContacts_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {
        }
    }
}

